<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Acces public</title>
    </head>
    <body>
      <c:import url="/menu.jsp" />
        <p>Vous n'avez pas acces a l'espace restreint : vous devez vous <a href="connexion">connecter</a> d'abord. </p>
    </body>
</html>