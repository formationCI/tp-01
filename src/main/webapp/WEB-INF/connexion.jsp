<%@ page import="java.util.List, java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x" %>
<%@ page isELIgnored="false"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>formation Servlet formulaire et session</title>
</head>

<body>


	<c:import url="/menu.jsp" />

<!-- lorsque les cookies bloquer cote client il faut le passer directement ds lurl grace a la bibliotheque c -->
<!-- <form method="post" action="/MonAppli4/connexion;jsessionid=HxHemXwQzTBT1Cp7iF6wDuYL.undefined"> -->

<%-- 	<form method="post" action="<c:url value="/connexion" />"> --%>
	<form method="post" action="connexion">
            <fieldset>
                <legend>IDENTIFICATION personne</legend>
               <table>
               	
	            <tr>
	              <td> 
	                <label for="email">Adresse email <span >*</span></label>
	              </td>
	              <td>
	                <input type="text" id="email" name="email" value="<c:out value="${personneVo.email}"/>" size="20" maxlength="60" />
	                <span class="erreur"><c:out value="${requestScope.erreurs.email}" /></span>
	              </td>
	            </tr>
	            <tr>
	              <td>
	                <label for="motdepasse">Mot de passe <span >*</span></label>
	              </td>
	              <td> 
	                <input type="password" id="motdepasse" name="motdepasse" value="" size="20" maxlength="20" />
	                <span class="erreur">${erreurs['motdepasse']}</span>
	              </td>
	           </tr>
	            <tr>
	             <td>
	                 <label for="memoire">Se souvenir</label>
                     <input type="checkbox" id="memoire" name="memoire" />
	             </td>
	             <td>  
	             	<input type="submit" value="Inscription"  />
	             </td>
	             </tr>
               </table>
<%--                 <c:if test="${!empty sessionScope.sessionPersonne}"> --%>
                    <%-- Si l'utilisateur existe en session, alors on affiche son adresse email. --%>
<%--                     <p >Vous etes connecte(e) avec l'adresse : ${sessionScope.sessionPersonne.email}</p> --%>
<%--                <br><a href="<c:url value="/deconnexion" />">deconnexion</a> --%>
<%--                 </c:if> --%>
            </fieldset>
        </form>
        <c:if test="${empty sessionScope.sessionPersonne && !empty requestScope.dateDernierConnexion}">
                	<p> Vous vous etes connecte pour la derniere fois le  : ${requestScope.dateDernierConnexion})</p>
         </c:if>
         
        <br>
        <c:if test="${ empty sessionScope.sessionPersonne}">
<!--         cette appel n'est pas correcte car supprime le contexte -->
<!--        <a href="/creationPersonneMVC"> Creer votre profil  </a>  -->
<%-- 			<a href="<c:url value="creationPersonneMVC" />">Creer votre profil</a> --%>
        </c:if>
        <c:out value="${requestScope.message}" />
<%--         <c:redirect url="/deconnexion"/> --%>
<%-- 		<jsp:forward page="/deconnexion.jsp"> --%>
	
</body>
</html>