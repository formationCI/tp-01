<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false"%>

 <c:if test="${!empty sessionScope.sessionPersonne}">
  Vous etes connecte(e) avec l'adresse : ${sessionScope.sessionPersonne.email} <br>
  <a href="<c:url value="/deconnexion" />">deconnexion</a>
  <a href="<c:url value="/private/private.jsp" />"> Acces a mon Espace </a>
 </c:if>

 <c:if test="${ empty sessionScope.sessionPersonne}">
 
		<a href="<c:url value="creationPersonneMVC" />">Creer votre profil</a>
		<a href="<c:url value="/visualisation" />"> visualiser tous les profils</a>
  </c:if>
  <br><br>
