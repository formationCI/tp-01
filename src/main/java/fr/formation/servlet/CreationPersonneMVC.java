package fr.formation.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.formation.buisness.traitement.CreationPersonneTraitement;
import fr.formation.commons.ObjetsCommuns;
import fr.formation.dao.DaoImpl;
import fr.formation.services.IServicePersonne;
import fr.formation.services.ServicePersonne;
import fr.formation.vo.PersonneVo;

public class CreationPersonneMVC extends HttpServlet {
	
	private IServicePersonne service; 
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		

		this.getServletContext().getRequestDispatcher( ObjetsCommuns.Url.VUE_CREATION_PERSONNE ).forward( req, resp );
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
	
		 	String email = req.getParameter( ObjetsCommuns.Param.CHAMP_EMAIL );
	        String motDePasse = req.getParameter( ObjetsCommuns.Param.CHAMP_PASS );
	        String confirmation = req.getParameter( ObjetsCommuns.Param.CHAMP_CONF );
	        String nom = req.getParameter( ObjetsCommuns.Param.CHAMP_NOM );
	        String prenom = req.getParameter( ObjetsCommuns.Param.CHAMP_PRENOM );

	        PersonneVo p = new PersonneVo();
	        p.setPrenom(prenom);
	        p.setNom(nom);
	        p.setEmail(email);
	        p.setMdp(motDePasse);
	        p.setConfirmationMdp(confirmation);
	        System.out.println(p);
	        PersonneVo personneRetour = null;
	        
	       CreationPersonneTraitement traitement = new CreationPersonneTraitement(p);
	       boolean isOK =  traitement.isValideChamps();
	       
	       HttpSession session = req.getSession();
	       
	       if (isOK){
	    	  
	    	   service =  new ServicePersonne(DaoImpl.getInstance());
	    	   personneRetour = service.enregistrerPersonne(p);
	    	   FactorySessionAndCookies.ajouterSession(session, req, resp, personneRetour);
	    	   
	        }else{
	        	personneRetour = p;
	        }
	        
	    req.setAttribute(ObjetsCommuns.Beans.PERSONNE_VO , personneRetour);        
		req.setAttribute( ObjetsCommuns.Beans.MAP_ERREURS, traitement.getErreurs() );
		if(isOK){
			 this.getServletContext().getRequestDispatcher( ObjetsCommuns.Url.VUE_ESPACE_PRIVE ).forward( req, resp );
			
		}else{
			 this.getServletContext().getRequestDispatcher( ObjetsCommuns.Url.VUE_CREATION_PERSONNE ).forward( req, resp );
		}
	   
	}
	

	
	
}
